@registration
Feature: Verify vehicle details from a file present in a given directory

  Scenario: Match vehicles with Registration,Make,Colour
    Given I am on homepage
    When I enter vehicle registration
    Then the make,colour,registration should match with file
