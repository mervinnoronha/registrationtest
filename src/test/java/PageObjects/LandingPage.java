package PageObjects;

import org.openqa.selenium.*;

public class LandingPage {

	private WebDriver driver;
	
	public LandingPage(WebDriver driver){
		this.driver=driver;
	}
	
	public RegistrationPage clickOnSignIn(){
		driver.findElement(By.className("button")).click();
		return new RegistrationPage(driver);
	}
	
}

