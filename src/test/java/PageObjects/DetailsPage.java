package PageObjects;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import Utility.Constants;
import DataObjects.FileScanner;
import DataObjects.ReadXlsx;

public class DetailsPage {
	private WebDriver driver;

	ReadXlsx reader =new ReadXlsx();
	static FileScanner fileScanner =new FileScanner();

	public DetailsPage(WebDriver driver) {
		this.driver = driver;
	}

	public void assertDetails() throws IOException {
		WebElement registration = driver.findElement(By.className("reg-mark"));
		WebElement make = driver.findElement(By.cssSelector("#pr3 > div > ul > li:nth-child(2) > span:nth-child(1)"));
		WebElement colour = driver.findElement(By.cssSelector("#pr3 > div > ul > li:nth-child(3) > span:nth-child(1)"));
		String expectedColour = expectedColour(); // get values from excel
		String expectedMake = expectedMakeOfVehicle(); // get values from excel
		String expectedRegistration =registrationNumber() ; // get values from excel

		//assert details match with those from excel sheet using Junit
		Assert.assertTrue(registration.getText().equals(expectedRegistration));
		Assert.assertTrue(make.getText().equals(expectedMake));
		Assert.assertTrue(colour.getText().equals(expectedColour));
	}

	public  String getExcelFilePathFromDirectory(){
		
		String path=fileScanner.getFilePathToRead(Constants.directoryName,Constants.mime);
		return path;
	}

	public String registrationNumber() throws IOException {

		String registrationNumber = reader.readExcelFileRegistration(getExcelFilePathFromDirectory());
		return registrationNumber;
	}

	public String expectedMakeOfVehicle() throws IOException {

		String make = reader.readExcelFileMake(getExcelFilePathFromDirectory());
		return make;
	}

	public String expectedColour() throws IOException {

		String colour = reader.readExcelFileColour(getExcelFilePathFromDirectory());
		return colour;
	}

}
