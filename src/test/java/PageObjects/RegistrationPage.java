package PageObjects;


import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import DataObjects.FileScanner;
import DataObjects.ReadXlsx;
import Utility.Constants;

public class RegistrationPage {
	private WebDriver driver;
		private String registrationNumber;
	static FileScanner fileScanner =new FileScanner();
	ReadXlsx reader =new ReadXlsx();

	public RegistrationPage(WebDriver driver) {

		this.driver=driver;
	}

	//get the path of the mime file selected
	public static String getExcelFilePathFromDirectory(){
		String path=fileScanner.getFilePathToRead(Constants.directoryName,Constants.mime);
		return path;
	}

	//A method to get the registration from excel
	public String registrationNumber() throws IOException {

		registrationNumber= reader.readExcelFileRegistration(getExcelFilePathFromDirectory());

		return registrationNumber;
	}

//To do iterate using scenario outline 
	public void enterRegistration() throws IOException{

		driver.findElement(By.id("Vrm")).sendKeys(registrationNumber());
	}

	public DetailsPage clickOnContinueBtn(){
		driver.findElement(By.name("Continue")).click();
		return new DetailsPage(driver);
	}



}
