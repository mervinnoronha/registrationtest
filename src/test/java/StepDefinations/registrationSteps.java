package StepDefinations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObjects.DetailsPage;
import PageObjects.LandingPage;
import PageObjects.RegistrationPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import Utility.Constants;

public class registrationSteps {
	private LandingPage lp;
	private RegistrationPage rp;
	private DetailsPage dp;
	private WebDriver driver;

	@Given("^I am on homepage$")
	public void i_am_on_homepage() throws Throwable {
		String exePath = "C:\\Users\\MEr#\\Downloads\\chromedriver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		driver = new ChromeDriver();
		driver.navigate().to(Constants.URL);
	}

	@When("^I enter vehicle registration$")
	public void i_enter_vehicle_registration() throws Throwable {
		lp = new LandingPage(driver);
		rp = lp.clickOnSignIn();
		rp.enterRegistration();
		dp = rp.clickOnContinueBtn();
	}

	@Then("^the make,colour,registration should match with file$")
	public void the_make_colour_registration_should_match_with_file() throws Throwable {
		dp.assertDetails();
	}
}
