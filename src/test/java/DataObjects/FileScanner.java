//Created a bean to get file information
//Using the Excel file found in scanned directory to pass values to selenium test
package DataObjects;

import java.io.File;
import java.io.IOException;
import com.google.common.io.Files;

public class FileScanner {
	private String name;
	private double size;
	private String extension;
	private String excelFileToRead;

	//To get teh file name
	public String getFileNames(String directoryName) {
		File directory = new File(directoryName);
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				name = file.getName();
			}
		}
		return name;
	}
// To get the file size
	public double getFileSize(String directoryName) {
		File directory = new File(directoryName);
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				size = file.length();
			}
		}
		return size;
	}

	
	//Using this method to identify the file to read from a list of files
	public String getFilePathToRead(String directoryName,String mime) {
		File directory = new File(directoryName);
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				name = file.getName();
				String extension = Files.getFileExtension(name);
				if(extension.contains(mime)){
					excelFileToRead=file.getAbsolutePath();
				}
			}}
		return excelFileToRead;
	}

	//To get the file extension
	public String getFileExtension(String directoryName) throws IOException {
		File directory = new File(directoryName);
		File[] flist = directory.listFiles();
		for (File file : flist) {
			if (file.isFile()) {
				name = file.getName();
				String extension = Files.getFileExtension(name);
			}
		}
		return extension ;
	}

}