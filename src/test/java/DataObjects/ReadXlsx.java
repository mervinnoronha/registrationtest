package DataObjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.xssf.usermodel.*;

public class ReadXlsx {
	String registration;
	String colour;
	String Make;

	public String readExcelFileRegistration(String Path) throws IOException{
		//Create input stream
		FileInputStream files=new FileInputStream(new File(Path));

		//instance workbook
		XSSFWorkbook workbook = new XSSFWorkbook(files);

		//Get the sheet from WB
		XSSFSheet sheet =workbook.getSheetAt(0);

		//Get rows present
		int rowCount=sheet.getLastRowNum();

		//Iterate through registrations
		for (int i = 1;i<=rowCount;i++){
			XSSFCell cell=sheet.getRow(i).getCell(1);
			registration=cell.getStringCellValue();
		}
		files.close();
		return registration;
	}

	public String readExcelFileColour(String Path) throws IOException{
		//Create input stream
		FileInputStream files=new FileInputStream(new File(Path));

		//instance workbook
		XSSFWorkbook workbook = new XSSFWorkbook(files);

		//Get the sheet from WB
		XSSFSheet sheet =workbook.getSheetAt(0);

		//Get rows present
		int rowCount=sheet.getLastRowNum();

		//Iterate through colour column
		for (int i = 1;i<=rowCount;i++){
			XSSFCell cell=sheet.getRow(i).getCell(2);
			colour=cell.getStringCellValue();
		}
		files.close();
		return colour;
	}

	public String readExcelFileMake(String Path) throws IOException{
		//Create input stream
		FileInputStream files=new FileInputStream(new File(Path));

		//instance workbook
		XSSFWorkbook workbook = new XSSFWorkbook(files);

		//Get the sheet from WB
		XSSFSheet sheet =workbook.getSheetAt(0);

		//Get rows present
		int rowCount=sheet.getLastRowNum();

		//Iterate through Make column
		for (int i = 1;i<=rowCount;i++){
			XSSFCell cell=sheet.getRow(i).getCell(3);
			registration=cell.getStringCellValue();
		}
		files.close();
		return registration;
	}
}